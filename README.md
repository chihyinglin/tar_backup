# tar_backup

## Intruduction
- backup script using **tar**
- support **incremental backup**

## usage
```
Usage:
  backup.sh -n <BACK_NAME> -s <BACK_SRC> -d <BACK_DST_DIR> [-h] [-f] [-w <BACK_WORK_DIR>]

params:
  -n: a unique backup name
  -s: the file/directory to backup
  -d: the directory to store backup data
  -f: force full backup for incremental backup
  -w: the work directory for this incremental backup
  -h: show this help
```

## example
### backup
1. initial
    - `mkdir src`
    - `mkdir backup`
    - `touch src/original_file`
    - ```
      .
      |-- backup
      `-- src
          `-- original_file                                
      ```
2. full backup at first time
    - `backup.sh -s src -d backup -n test1 -f`
    - ```
      .
      |-- backup
      |   |-- test1_INCR_20211222094452_BASE.tgz
      `-- src
          `-- original_file
      ```
3. incremental backup 1
    - `touch src/new_file`
    - `backup.sh -s src -d backup -n test1`
    - ```
      .
      |-- backup
      |   |-- test1_INCR_20211222094452_BASE.tgz
      |   `-- test1_INCR_20211222094641.tgz
      `-- src
          |-- new_file
          `-- original_file
      ```
### restore
1. delete the data
    - `rm -rf src`
    - ```
      .
      `-- backup
          |-- test1_INCR_20211222094452_BASE.tgz
          `-- test1_INCR_20211222094641.tgz
      ```
3. restore 
    - use `-g /dev/null` to restore and handle deleted data
      - `tar -g /dev/null -zxvf backup/test1_INCR_20211222094452_BASE.tgz`
      - `tar -g /dev/null -zxvf backup/test1_INCR_20211222094641.tgz`
    - ```
      .
      |-- backup
      |   |-- test1_INCR_20211222094452_BASE.tgz
      |   `-- test1_INCR_20211222094641.tgz
      `-- src
          |-- new_file
          `-- original_file
      ```