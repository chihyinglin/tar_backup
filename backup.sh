#!/bin/bash

# Exit immediately if a command exits with a non-zero status.
set -e

##########
# settings
##########
BACK_NAME=
BACK_SRC=
BACK_DST_DIR=
BACK_FULL=false
BACK_WORK_DIR=/tmp/tar_backup/
mkdir -p $BACK_WORK_DIR

##########
# functions
##########
function show_usage {
  echo ""
  echo "Usage:"
  echo "  $0 -n <BACK_NAME> -s <BACK_SRC> -d <BACK_DST_DIR> [-h] [-f] [-w <BACK_WORK_DIR>]"
  echo ""
  echo "params:"
  echo "  -n: a unique backup name"
  echo "  -s: the file/directory to backup"
  echo "  -d: the directory to store backup data"
  echo "  -f: force full backup for incremental backup"
  echo "  -w: the work directory for this incremental backup"
  echo "  -h: show this help"
  exit 1
}


##########
# main
##########

# args
while getopts s:d:w:n:ifh flag
do
    case "${flag}" in
        n) BACK_NAME=$OPTARG;;
        #s) BACK_SRC=$(readlink -f $OPTARG);;
        s) BACK_SRC=$OPTARG;;
        #d) BACK_DST_DIR=$(readlink -f $OPTARG);;
        d) BACK_DST_DIR=$OPTARG;;
        #w) BACK_WORK_DIR=$(readlink -f $OPTARG);;
        w) BACK_WORK_DIR=$OPTARG;;
        f) BACK_FULL=true;;
        h) show_usage;;
    esac
done
echo BACK_NAME=$BACK_NAME
echo BACK_SRC=$BACK_SRC
echo BACK_DST_DIR=$BACK_DST_DIR
echo BACK_WORK_DIR=$BACK_WORK_DIR
echo
# check empty
if [ -z "$BACK_NAME" ] || [ -z "$BACK_SRC" ] || [ -z "$BACK_DST_DIR" ]; then show_usage; fi

# do the backup
ts=$(date +"%Y%m%d%H%M%S")
shadow_file=${BACK_WORK_DIR}/shadow_${BACK_NAME}
echo shadow_file=$shadow_file
if [ "$BACK_FULL" == true ]; then
  echo "Force full backup"
  rm -rf $shadow_file
fi
if [ ! -e "$shadow_file" ]; then
  backup_file=${BACK_DST_DIR}/${BACK_NAME}_INCR_${ts}_BASE.tgz
else
  backup_file=${BACK_DST_DIR}/${BACK_NAME}_INCR_${ts}.tgz
fi
echo backup_file=$backup_file
cmd="tar -g ${shadow_file} -zcvf ${backup_file} ${BACK_SRC}"
echo cmd: $cmd
$cmd


# to restore incremental backup data:
#   tar -g /dev/null -zxvf XXXX_INCR_BASE_202101010101.tgz
#   tar -g /dev/null -zxvf XXXX_INCR__202101010201.tgz
#   tar -g /dev/null -zxvf XXXX_INCR__202101010301.tgz
#   tar -g /dev/null -zxvf XXXX_INCR__202101010401.tgz
#   ...
